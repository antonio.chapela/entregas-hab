
let users = [
    {
        email: 'prueba@hackaflight.com',
        password: '$2b$10$U0MuOrhWG5oJ6ABwvllYEO5ZdHKPSAl.5ShvFOMMV9jTjgu2t2JfC'
    }
];

let id = 1;

const getUser = (email) => {
    const matchEmail = user => user.email === email;

    return users.find( matchEmail );
}

const saveUser = (email, password) => {
    users.push({
        id: id++,
        email,
        password,
    })
}


module.exports = {
    getUser,
    saveUser
}
