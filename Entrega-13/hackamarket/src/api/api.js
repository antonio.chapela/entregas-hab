// Declarando cosas que instalé

const cors = require('cors')
const bodyParser = require('body-parser')
const mysql = require('mysql')
const express = require('express')
const app = express()

// Cosas que usa APP

app.use(cors())
app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())

// conexión a la BBDD
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '123456',
    database: 'notas'
})

// realizando conexión a BBDD
connection.connect(error => {
    if(error) throw error
    console.log('DATABASE CONECTADA :)')
})

// PUERTO DE CONEXIÓN DEL SERVICIO
const PORT = 3308

// CONEXIÓN DEL SERVICIO
app.listen(PORT, () => console.log('API CONECTADA :)'))


// RECOGER TODOS LOS CLIETES DE LA BASE
app.get('/', (req, res) => {
    res.send('Hola')
})

// RECOGIENDO TODOS LOS CLIENTES DE LA BASE DE DATOS
app.get('/clientes', (req, res) => {
    // secuencia sql
    const sql='SELECT * FROM listaclientes'

    // conexion a BBDD
    connection.query(sql, (error, results) => {
        if(error) throw error
        if(results.length > 0) {
            res.json(results)
        }
        else{
            // si no hay clientes mostramos este dato
            console.log('No hay clientes que mostrar. :(')
        }
    })
})

// RECOGIENDO TODOS LOS PRODUCTOS DE LA BASE DE DATOS
app.get('/productos', (req, res) => {
    // secuencia sql
    const sql='SELECT * FROM listaproductos'

    // conexion a BBDD
    connection.query(sql, (error, results) => {
        if(error) throw error
        if(results.length > 0) {
            res.json(results)
        }
        else{
            // si no hay clientes mostramos este dato
            console.log('No hay productos que mostrar. :(')
        }
    })
})

// AÑADIR CLIENTES A LA BBDD
app.post('/add',(req, res) => {

    // SECUENCIA SQL
    const sql = "INSERT INTO listaclientes SET ?"

    // OBJETO DE DATOS DEL NUEVO CLIENTE
    const nuevoCliente = {
        nombre: req.body.nombre,
        usuario: req.body.usuario,
        password: req.body.password,
        email: req.body.email,
        foto: req.body.foto
    }

    // CONEXIÓN A BBDD
    connection.query( sql, nuevoCliente, error => {
        if(error) throw error
        console.log('Cliente creado con éxito :D')
    })

})

// ACTUALIZANDO CLIENTES EN LA BBDD
app.put('/update/:id', (req, res) => {

    //DATOS QUE RECIBIMOS DE LA VISTA
    const id = req.params.id
    const nombre = req.body.nombre
    const usuario = req.body.usuario
    const password = req.body.password
    const email = req.body.email

    //SECUENCIA SQL
    const sql = `UPDATE listaclientes SET nombre='${nombre}', usuario='${usuario}', password='${password}', email='${email}' WHERE id=${id}`
    
    //CONEXION A LA BBDD
    connection.query( sql, error => {
        if(error) throw error
        console.log('Cliente actualizado con éxito :D')
    })
})

// BORRANDO CLIENTES EN LA BBDD
app.delete('/delete/:id', (req, res) => {
     
    //DATOS QUE RECIBIMOS DE LA VISTA
    const id = req.params.id

    //SECUENCIA SQL
    const sql = `DELETE FROM listaclientes WHERE id=${id}`
    
    //CONEXION A LA BBDD
    connection.query( sql, error => {
        if(error) throw error
        console.log('Cliente borrado con éxito :(')
    })

})