// Variables

const botones = document.querySelectorAll('.boton')
const carroHotel = document.querySelector('#list')

// Funciones

const crearHotel = hotel => {
    const cardHotel = document.createElement('div')
    cardHotel.id = 'cart-card-'
    cardHotel.className = 'cart-card'
    cardHotel.innerHTML = `
        <img src="${hotel.imagen}" alt="${hotel.nombre} imagen"
        <section>
            <h2 class="nameP">${hotel.nombre}</h2>
            <span class="precioP">${hotel.precio}</span>
            <button class="delete">Eliminar</button>
        </section>
    `
    carroHotel.appendChild(cardHotel)
}
const cleanHotel = e => {
    if(e.target.classList.contains('delete')){
    e.target.parentElement.remove()
    }
}
const extraerDatosHotel = hotel => {
    const datosHotel = {
        imagen: hotel.querySelector('img').src,
        nombre: hotel.querySelector('h2').innerText,
        precio: hotel.querySelector('.price').innerText,
        id: hotel.getAttribute('data-id')
    }
    console.log(datosHotel)
    crearHotel(datosHotel)
}

const enviarHotel = e => {
    e.preventDefault()
    const elemento = e.target.parentElement.parentElement
    extraerDatosHotel(elemento)
}


// Liseners

botones.forEach(boton => {
    boton.addEventListener('click', enviarHotel)
})

carroHotel.addEventListener('click', cleanHotel)