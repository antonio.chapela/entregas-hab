/**
 * Un cliente nos pide realizar un sistema para gestionar eventos culturales.
 * Necesita dar de alta eventos, que pueden ser de tipo 'concierto', 'teatro' o
 * 'monólogo'. Cada uno se caracteriza por un 'nombre', 'aforo' y 'artista'.
 * Opcionalmente pueden incluir una descripción.
 *
 * El cliente necesitará una API REST para añadir eventos y poder obtener
 * una lista de los existentes.
 *
 * El objetivo del ejercicio es que traduzcas estos requisitos a una descripción
 * técnica, esto es, decidir qué endpoints hacen falta, qué parámetros y cuáles
 * son los código de error a devolver
 *
 * Notas:
 *    - el conocimiento necesario para realizarlo es el adquirido hasta la clase del
 *      miércoles
 *    - llega con un endpoint GET y otro POST
 *    - el almacenamiento será en memoria, por tanto cuando se cierre el servidor
 *      se perderán los datos. De momento es aceptable esto.
 *
 */

const express = require("express");
const bodyParser = require("body-parser");
const app = express();

//  CORS

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:3000");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

// BODY PARSER

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// GET

let event = [];

app.get("/event", (req, res) => {
  res.json(event);
});

// POST

app.post("/event", (req, res) => {
  let data = {
    tipo: req.body.tipo,
    nombre: req.body.nombre,
    aforo: req.body.aforo,
    artista: req.body.artista,
  };

  // COMPROBACIÓN ERRORES

  if (
    req.body.tipo !== "concierto" &&
    req.body.tipo !== "teatro" &&
    req.body.tipo !== "monólogo"
  ) {
    res
      .status(400)
      .send("ATENCIÓN: solo puede ser tipo concierto, teatro o monólogo");
    return;
  }

  if (
    data.tipo === undefined ||
    data.nombre === undefined ||
    data.aforo === undefined ||
    data.artista === undefined
  ) {
    res
      .status(400)
      .send(
        "ATENCIÓN: Debe añadir todos los campos: tipo, nombre, aforo y artista"
      );
    return;
  }

  let search = event.filter((event) => event.nombre === data.nombre);

  if (search.length !== 0) {
    res.status(409).send("ATENCIÓN: Evento con nombre repetido");
    return;
  } else {
    event.push(data);
    res.send();
  }
});

app.listen(8000);
