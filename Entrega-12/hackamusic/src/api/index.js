import config from './config.js'
const axios = require('axios').default;

const apiKey = config.apiKey
const BASE_URL = `https://ws.audioscrobbler.com/`
const URL_GEO = `2.0/?method=geo.gettopartists&country=spain&api_key=deca6c4006a5bcddea12bafdb1afa1c1&format=json`
const URL_TAG = `2.0/?method=chart.gettoptags&api_key=deca6c4006a5bcddea12bafdb1afa1c1&format=json`
const URL_TRACK = `2.0/?method=geo.gettoptracks&country=spain&api_key=deca6c4006a5bcddea12bafdb1afa1c1&format=json`


async function getArtists() {
    try {
        const response = await axios.get(`${BASE_URL}${URL_GEO}`);
        console.log(response);
        return (response)
    } catch (error) {
        console.error(error);
    }
}
async function getTopTracks() {
    try {
        const response = await axios.get(`${BASE_URL}${URL_TRACK}`);
        console.log(response);
        return (response)
    } catch (error) {
        console.error(error);
    }
}
async function getTopTags() {
    try {
        const response = await axios.get(`${BASE_URL}${URL_TAG}`);
        console.log(response);
        return (response)
    } catch (error) {
        console.error(error);
    }
}

export default {
    getArtists,
    getTopTracks,
    getTopTags
}