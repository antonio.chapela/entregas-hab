const mysql = require("mysql2/promise");

async function connection() {
  return await mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "123456",
    database: "notas",
  });
}

module.exports = {
  connection,
};
