/**
 * El objetivo de este ejercicio es implementar una aplicación de consola
 * para calcular la cuota mensual de un préstamo. Para ello, la aplicación debe:
 *     - usar una librería para realizar los cálculos necesarios (buscad en npmjs
 *       por las palabras loan, mortgage o payoff)
 *     - recibir por línea de comandos los argumentos necesarios para realizar los
 *       cálculos. Para esto podéis usar directamente la API de NodeJS (process.argv)
 *       o bien usar una librería como commander o yargs 
 *     - el resultado debe aparecer en la consola
 * 
 * 
 * Escribe $ node entrega-7.js <-espacio-> cantidad total <-espacio-> interes <-espacio-> tiempo en meses
 */

var AmortizeJS = require('amortizejs').Calculator;
const chalk = require('chalk');

const balance = process.argv[2];
const apr = process.argv[3];
const loanTerm = process.argv[4];

var mortgage = AmortizeJS.calculate({
    method:   'mortgage',
    apr:      apr, 
    balance:  balance,    
    loanTerm: loanTerm,         
    startDate: new Date()
});

var anno = (loanTerm)/12

console.log( chalk`{green.underline Vas a pagar al mes:} {inverse.green.bold ${mortgage.periodicPayment.toFixed(2)}€} (con un interés de ${process.argv[3]}%) durante  ${anno.toFixed(0)} años (${process.argv[4]} meses)` );
console.log( `Al final vas a pagar un total de: ${mortgage.totalPayment.toFixed(0)}€ (de los ${process.argv[2]}€ iniciales)`);
console.log( `Por lo tanto terminas de pagar el: ${mortgage.endDate}`);
